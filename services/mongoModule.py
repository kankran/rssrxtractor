from pymongo import MongoClient
client = MongoClient('localhost', 27017)
db = client.rssdb


def insert( data  , collection ):
	c_Name = db[collection]
	return c_Name.insert( data )


def fetchOne( query , collection ):
	c_Name = db[ collection ]
	result = c_Name.find_one(  query )
	return result
