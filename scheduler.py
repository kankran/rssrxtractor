import json
import requests
import time
import schedule


def rssCall():
	try:
		endpoint = '/xmlparse'
		url="http://localhost:5000"
		headers = {'content-type': 'application/json'}
		with open("feed_url.json", "r") as jsonFile:
			data = json.load(jsonFile)
	  	url_list = data["feed_url"]
	  	res=requests.request("POST",url+endpoint,data=json.dumps({"feed_url_list":url_list}),headers=headers,timeout=120).text
	  	return res
	except Exception as e:
		return {"message": str(e)}



def schedulerCall():
	"""scheduler function to start this script every day at given time"""
	#schedule.every(15).minutes.do(rssCall)
	schedule.every().hour.do(rssCall)
	#infoLog("cron job working")
	#schedule.every().day.at("18:10").do(doLogs)
	while 1:
	    schedule.run_pending()
	    time.sleep(1)

if __name__ =="__main__":
    #schedulerCall()
    rssCall()  
