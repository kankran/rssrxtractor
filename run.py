# -*- coding: utf-8 -*-
#python in built module
from services.logFile import infoLog, errorLog, criticalLog
from flask import Flask, jsonify, request
import os
import time
import json
import requests
import datetime
from services import mongoModule

app = Flask(__name__)

def checkDuplicateRss(data,collection="rssdb"):
  try:
    resData=mongoModule.fetchOne(data,collection)
    if resData is None:
      result=mongoModule.insert( data  , collection )
      infoLog(result)
      return result
    else:
      pass
  except Exception as e:
    errorLog(str(e))
    return jsonify({"message": str(e)}), 400

def fetchRssUrl():
  try:
    url="http://gmpnews.hotify.com:5000/fetchurl"
    headers = {'content-type': 'application/json','authorization': "bearer eyJhbGciOiJzaGEyMjQiLCJ0eXBlIjoiSldUIn0=.eyJmcm9tIjoiYW5rdXIiLCJpYXQiOjE0OTQwNTc5OTc0OTV9.M2E3MzNjN2ZhNTAzNjNjOTQ2YTE0NDNkYzMzYTY0ZWZlMzg5NmRmZWNkOGRjMjc5OTkyZDljNmM="}
    data=mongoModule.db.rssdb.find({"created_at":datetime.datetime.today().strftime('%Y-%m-%d')})
    res=list(data)
    for link in res:
      infoLog(requests.request("POST",url,data=json.dumps({"url":link['link']}),headers=headers).text)
      time.sleep(1)
  except Exception as e:
            return jsonify({"message": str(e)}), 400





@app.route("/xmlparse",methods=['POST'])
def xmlParse():
  feed_url_list=request.json.get('feed_url_list')
  import urllib2
  from xml.etree import ElementTree as etree
  #reddit parse
  for url in feed_url_list:
    reddit_file = urllib2.urlopen(url)
    infoLog("feed url: "+url)
    #convert to string:
    reddit_data = reddit_file.read()
    #print reddit_data
    #close file because we dont need it anymore:
    reddit_file.close()

    #entire feed
    reddit_root = etree.fromstring(reddit_data)
    item = reddit_root.findall('channel/item')
    
    for entry in item:   
        #get description, url, and thumbnail
        title = entry.findtext('title')
        link = entry.findtext('link')
        pubDate = entry.findtext('pubDate')
        payload={
                  "title":title,
                  "link":link,
                  "pubDate":pubDate,
                  "created_at":datetime.datetime.today().strftime('%Y-%m-%d')
        }
        infoLog(payload)
        checkDuplicateRss(payload) 
  fetchRssUrl()
  return jsonify({"result":"all rss feed added successfully to db"})

#maintain url file 
def updateFeedUrlFile(url):
  with open("feed_url.json", "r") as jsonFile:
    data = json.load(jsonFile)
  url_list = data["feed_url"]
  if url not in  url_list:
    url_list.append(url)
    data["feed_url"] = url_list
  with open("feed_url.json", "w") as jsonFile:
      json.dump(data, jsonFile)
  return url_list


@app.route("/feedurl",methods=['POST'])
def feedTrimmer():
  try:
    if request.method == 'POST':
      url=request.json.get('url',None)
      infoLog(url)
      updateFeedUrlFile(url)
      return jsonify({"message": "No added successfully"})
  except Exception as e:
            errorLog(str(e))
            return jsonify({"message": str(e)}), 400

if __name__ == "__main__":
  app.run( host="0.0.0.0", debug=True, port=5000)



